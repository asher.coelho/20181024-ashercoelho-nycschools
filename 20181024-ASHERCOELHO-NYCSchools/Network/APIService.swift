//
//  APIService.swift
//  20181024-ASHERCOELHO-NYCSchools
//
//  Created by Asher Coelho on 10/24/18.
//  Copyright © 2018 AsherCoelho. All rights reserved.
//

import Foundation

protocol APIServiceProtocol {    
    func fetchSchoolsWithScores(complete: @escaping ( _ schools: Schools?, _ error: Error? )->() )
}

class APIService: APIServiceProtocol {
    
    private func fetchSchools(complete: @escaping ( _ schools: Schools?, _ error: Error? )->() ) {
        let url = URL(string: "https://data.cityofnewyork.us/resource/97mf-9njv.json")!
        let task = URLSession.shared.schoolsTask(with: url) { (schools, response, error) in
            complete(schools, error)
        }
        task.resume()
    }
    
    private func fetchSchoolsSATScores(complete: @escaping ( _ satScores: SATScores?, _ error: Error? )->() ) {
        let url = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")!
        let task = URLSession.shared.schoolsSATScoresTask(with: url) { (satScores, response, error) in
            complete(satScores, error)
        }
        task.resume()
    }
    
    //TODO: Improve error handling
    public func fetchSchoolsWithScores(complete: @escaping ( _ schools: Schools?, _ error: Error? )->() ) {
        
        fetchSchools(complete: { [weak self] (schools, error) in
            guard let strongSelf = self else { return }
            guard let schools = schools else { return complete(nil, error) }
            
            strongSelf.fetchSchoolsSATScores(complete: { (satScores, error) in
                guard let satScores = satScores else { return complete(nil, error) }
                
                let scoresDict = satScores.reduce(into: [String: SATScore]()) { $0[$1.dbn] = $1 }
                schools.forEach { $0.satScore = scoresDict[$0.dbn] }
                
                complete(schools, error)
            })
        })
    }
}

// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            do {
                let t = try newJSONDecoder().decode(T.self, from: data)
                completionHandler(t, response, nil)
            } catch {
                completionHandler(nil, response, error)
            }
        }
    }
    
    func schoolsTask(with url: URL, completionHandler: @escaping (Schools?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
