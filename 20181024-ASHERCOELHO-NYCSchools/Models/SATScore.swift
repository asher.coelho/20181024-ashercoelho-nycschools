//
//  SchoolSATScore.swift
//  20181024-ASHERCOELHO-NYCSchools
//
//  Created by Asher Coelho on 10/24/18.
//  Copyright © 2018 AsherCoelho. All rights reserved.
//

import Foundation

typealias SATScores = [SATScore]

class SATScore: Codable {
    let dbn: String
    let numOfSatTestTakers: String?
    let satCriticalReadingAvgScore: String?
    let satMathAvgScore: String?
    let satWritingAvgScore: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
    
    init(dbn: String, numOfSatTestTakers: String? = nil, satCriticalReadingAvgScore: String? = nil, satMathAvgScore: String? = nil, satWritingAvgScore: String? = nil) {
        self.dbn = dbn
        self.numOfSatTestTakers = numOfSatTestTakers
        self.satCriticalReadingAvgScore = satCriticalReadingAvgScore
        self.satMathAvgScore = satMathAvgScore
        self.satWritingAvgScore = satWritingAvgScore
    }
}

// MARK: Convenience initializers and mutators

extension SATScore {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(SATScore.self, from: data)
        self.init(dbn: me.dbn, numOfSatTestTakers: me.numOfSatTestTakers, satCriticalReadingAvgScore: me.satCriticalReadingAvgScore, satMathAvgScore: me.satMathAvgScore, satWritingAvgScore: me.satWritingAvgScore)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == SATScores.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SATScores.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
// MARK: - URLSession response handlers

extension URLSession {
    fileprivate func codableTask<T: Codable>(with url: URL, completionHandler: @escaping (T?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(nil, response, error)
                return
            }
            do {
                let t = try newJSONDecoder().decode(T.self, from: data)
                completionHandler(t, response, nil)
            } catch {
                completionHandler(nil, response, error)
            }
        }
    }
    
    func schoolsSATScoresTask(with url: URL, completionHandler: @escaping (SATScores?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return self.codableTask(with: url, completionHandler: completionHandler)
    }
}
