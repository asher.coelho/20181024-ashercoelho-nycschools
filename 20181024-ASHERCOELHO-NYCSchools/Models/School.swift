//
//  School.swift
//  20181024-ASHERCOELHO-NYCSchools
//
//  Created by Asher Coelho on 10/24/18.
//  Copyright © 2018 AsherCoelho. All rights reserved.
//

import Foundation

typealias Schools = [School]

class School: Codable {
    let dbn: String
    let schoolName: String?
    let grades2018: String?
    let borough: String?
    let totalStudents: String?
    let website: String?
    let phoneNumber: String?
    let overviewParagraph: String?
    
    var satScore: SATScore?
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case grades2018 = "grades2018"
        case borough = "borough"
        case totalStudents = "total_students"
        case website = "website"
        case phoneNumber = "phone_number"
        case overviewParagraph = "overview_paragraph"
    }
    
    init(dbn: String, schoolName: String? = nil, grades2018: String? = nil, borough: String? = nil, totalStudents: String? = nil, website: String? = nil, phoneNumber: String? = nil, overviewParagraph: String? = nil) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.grades2018 = grades2018
        self.borough = borough
        self.totalStudents = totalStudents
        self.website = website
        self.phoneNumber = phoneNumber
        self.overviewParagraph = overviewParagraph
    }
}

// MARK: Convenience initializers and mutators

extension School {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(School.self, from: data)
        self.init(dbn: me.dbn, schoolName: me.schoolName, grades2018: me.grades2018, borough: me.borough, totalStudents: me.totalStudents, website: me.website, phoneNumber: me.phoneNumber, overviewParagraph: me.overviewParagraph)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

extension Array where Element == Schools.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(Schools.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}
