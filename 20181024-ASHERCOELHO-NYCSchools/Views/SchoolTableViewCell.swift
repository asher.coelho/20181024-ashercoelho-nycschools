//
//  SchoolTableViewCell.swift
//  20181024-ASHERCOELHO-NYCSchools
//
//  Created by Asher Coelho on 10/24/18.
//  Copyright © 2018 AsherCoelho. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    @IBOutlet public var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

// MARK: - schoolViewModelView
extension SchoolTableViewCell: SchoolViewModelView {
    
    public var schoolTitleLabel: UILabel { return nameLabel }
}

