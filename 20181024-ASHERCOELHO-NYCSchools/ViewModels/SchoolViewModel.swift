//
//  SchoolDetailViewController.swift
//  20181024-ASHERCOELHO-NYCSchools
//
//  Created by Asher Coelho on 10/24/18.
//  Copyright © 2018 AsherCoelho. All rights reserved.
//

import Foundation
import UIKit

@objc public protocol SchoolViewModelView {
    @objc optional var schoolTitleLabel: UILabel { get }
    @objc optional var schoolIdLabel: UILabel { get }
    @objc optional var gradesLabel: UILabel { get }
    @objc optional var boroughLabel: UILabel { get }
    @objc optional var studentsLabel: UILabel { get }
    @objc optional var testTakersLabel: UILabel { get }
    @objc optional var criticalThinkingScoreLabel: UILabel { get }
    @objc optional var mathScoreLabel: UILabel { get }
    @objc optional var writingScoreLabel: UILabel { get }
    @objc optional var websiteLabel: UILabel { get }
    @objc optional var phoneNumberLabel: UILabel { get }
    @objc optional var descriptionLabel: UILabel { get }
}

class SchoolViewModel {
    
    
    // MARK: - Instance Properties
    public let school: School
    
    public let schoolName: String
    public let schoolId: String
    public let grades: String
    public let borough: String
    public let students: String
    public let testTakers: String
    public let criticalThinkingScore: String
    public let mathScore: String
    public let writingScore: String
    public let website: String
    public let phoneNumber: String
    public let overview: String
    
    
    public init(school: School) {
        self.school = school
        
        schoolId = school.dbn
        schoolName = school.schoolName ?? "n/a" 
        grades = school.grades2018 ?? "n/a"
        borough = school.borough ?? "n/a"
        students = school.totalStudents ?? "n/a"
        website = school.website ?? "n/a"
        phoneNumber = school.phoneNumber ?? "n/a"
        overview = "\"" + (school.overviewParagraph ?? "n/a") + "\""
        
        let satScore = school.satScore ?? SATScore(dbn: school.dbn)
        testTakers = satScore.numOfSatTestTakers ?? "n/a"
        criticalThinkingScore = satScore.satCriticalReadingAvgScore ?? "n/a"
        mathScore = satScore.satMathAvgScore ?? "n/a"
        writingScore = satScore.satWritingAvgScore ?? "n/a"
    }
    
}

extension SchoolViewModel {
    public func configure(_ view: SchoolViewModelView) {
        view.schoolTitleLabel?.text = schoolName
        
        view.schoolIdLabel?.text = schoolId
        view.gradesLabel?.text = grades
        view.boroughLabel?.text = borough
        view.studentsLabel?.text = students
        view.testTakersLabel?.text = testTakers
        view.criticalThinkingScoreLabel?.text = criticalThinkingScore
        view.mathScoreLabel?.text = mathScore
        view.writingScoreLabel?.text = writingScore
        view.websiteLabel?.text = website
        view.phoneNumberLabel?.text = phoneNumber
        view.descriptionLabel?.text = overview
        
    }
}
