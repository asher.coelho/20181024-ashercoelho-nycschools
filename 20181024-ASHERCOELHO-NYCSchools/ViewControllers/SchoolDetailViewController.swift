//
//  SchoolDetailViewController.swift
//  20181024-ASHERCOELHO-NYCSchools
//
//  Created by Asher Coelho on 10/24/18.
//  Copyright © 2018 AsherCoelho. All rights reserved.
//

import Foundation
import UIKit

class SchoolDetailViewController: UITableViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var schoolId: UILabel!
    @IBOutlet weak var grades: UILabel!
    @IBOutlet weak var borough: UILabel!
    @IBOutlet weak var students: UILabel!
    @IBOutlet weak var testTakers: UILabel!
    @IBOutlet weak var criticalThinkingScore: UILabel!
    @IBOutlet weak var mathScore: UILabel!
    @IBOutlet weak var writingScore: UILabel!
    @IBOutlet weak var website: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    // MARK: - Injections
    public var schoolViewModel: SchoolViewModel!
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        schoolViewModel?.configure(self)
        self.title = schoolViewModel.schoolName + "🏫"
        
        //TODO: Link to website and call functions
        //TODO: Better display of cells's data
    }
    
    // MARK: - TableView Delegate & DataSource
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

// MARK: - SchoolViewModelView
extension SchoolDetailViewController: SchoolViewModelView {
    
    public var schoolIdLabel: UILabel { return schoolId }
    public var gradesLabel: UILabel { return grades }
    public var boroughLabel: UILabel { return borough }
    public var studentsLabel: UILabel { return students }
    public var testTakersLabel: UILabel { return testTakers }
    public var criticalThinkingScoreLabel: UILabel { return criticalThinkingScore }
    public var mathScoreLabel: UILabel { return mathScore }
    public var writingScoreLabel: UILabel { return writingScore }
    public var websiteLabel: UILabel { return website }
    public var phoneNumberLabel: UILabel { return phoneNumber }
    public var descriptionLabel: UILabel { return overviewLabel }
}

