//
//  ViewController.swift
//  20181024-ASHERCOELHO-NYCSchools
//
//  Created by Asher Coelho on 10/24/18.
//  Copyright © 2018 AsherCoelho. All rights reserved.
//

import UIKit

class SchoolsViewController: UIViewController {

    // MARK: - Injections
    public var apiService: APIServiceProtocol!
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Instace Properties
    let searchController = UISearchController(searchResultsController: nil)
    internal var schoolViewModels: [SchoolViewModel] = []
    internal var filteredSchoolViewModels = [SchoolViewModel]()
    internal var groupedViewModels = [(key: "", value: [SchoolViewModel]())]
    
    var predicate: (SchoolViewModel) -> String = {
        let c = String($0.schoolName.prefix(1))
        return ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".contains(c) ) ? c : "?"
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        
        loadSchools()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //
    }

    // MARK: - Methods
    internal func  initView() {
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Schools"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    @objc internal func loadSchools() {
        self.tableView.refreshControl?.beginRefreshing()
        
        apiService.fetchSchoolsWithScores(complete: { [weak self] (schools, error) in
            guard let strongSelf = self else { return }
            guard let schools = schools else { return print(error ?? "Something went wrong") }
            
            let schoolViewModels = schools.map{ SchoolViewModel(school: $0) }
            strongSelf.schoolViewModels = schoolViewModels
            
            strongSelf.groupedViewModels = Dictionary(grouping: schoolViewModels, by: strongSelf.predicate).sorted { $0.0 < $1.0 }
            
            
            DispatchQueue.main.async(execute: { () -> Void in
                strongSelf.tableView.refreshControl?.endRefreshing()
                strongSelf.tableView.reloadData()
            })
        })
    }
    
    private func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredSchoolViewModels = schoolViewModels.filter({( schoolVM : SchoolViewModel) -> Bool in
            return schoolVM.schoolName.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    private func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let viewController = segue.destination as? SchoolDetailViewController else { return }
        let indexPath = tableView.indexPathForSelectedRow!
        
        let schoolViewModel: SchoolViewModel
        if isFiltering() {
            schoolViewModel = filteredSchoolViewModels[indexPath.row]
        } else {
            schoolViewModel = groupedViewModels[indexPath.section].value[indexPath.row]
        }
        viewController.schoolViewModel = schoolViewModel
    }
}

//MARK: UITableViewDataSource
extension SchoolsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isFiltering() { return 1 }
        return groupedViewModels.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() { return filteredSchoolViewModels.count }
        return groupedViewModels[section].value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SchoolCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SchoolTableViewCell
        
        let schoolViewModel: SchoolViewModel
        if isFiltering() {
            schoolViewModel = filteredSchoolViewModels[indexPath.row]
        } else {
            schoolViewModel = groupedViewModels[indexPath.section].value[indexPath.row]
        }
        
        schoolViewModel.configure(cell)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isFiltering() { return nil }
        return groupedViewModels[section].key
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isFiltering() { return nil }
        return groupedViewModels.map{ $0.key }
    }
    
}

// MARK: - UISearchResultsUpdating Delegate
extension SchoolsViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

